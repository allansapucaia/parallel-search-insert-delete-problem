from threading_cleanup import *
from collections import deque
#Variables used in the commucation between modules ( similar to multiple-producers single-consumer problem)
commands_lock = Semaphore(1)
commands = deque() #queue of animation commands (dicts) created by the threads 
animation_locks = dict() # the threads sleep waiting for the animation to complete
done = 0 #finish the animation module

def insertCommand(command):
    commands_lock.wait()
    commands.append(command)
    commands_lock.signal()

def getNextCommand():
    ret = None
    commands_lock.wait()
    if len(commands) >= 1:
        ret = commands.popleft()
    commands_lock.signal()
    return ret
