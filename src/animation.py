#This file is responsable for animating the program and unlocking threads waiting for animations
import sys, pygame, os, random
import sharedVariables

from threading_cleanup import *
from pygame.locals import *



#--------------------------------------------global variables------------------------------------------------
allSprites = pygame.sprite.Group()
screenSize = (1000, 700);
maxNumberOfNodes = 5
inserterEscapingPosition = (screenSize[0]*2, screenSize[1]*1.5/2)
searcherEscapingPosition = (screenSize[0]*2, screenSize[1]*1.5/2)
deleterEscapingPosition = (screenSize[0]*2, int(screenSize[1]*0.2))
nodesPositions = []
insertersWaitingPositons = []
deletersWaitingPositions = []

indexFromThreadId = dict()

for ku in range(maxNumberOfNodes):
    nodesPositions.append((int(screenSize[0]*0.2 + screenSize[0]*ku/(1.1*maxNumberOfNodes)),screenSize[1]*0.3))
    insertersWaitingPositons.append((int(screenSize[0]*0.2 + screenSize[0]*ku/(1.1*maxNumberOfNodes)),int(screenSize[1]*0.1)))
    deletersWaitingPositions.append((int(screenSize[0]*0.2 + screenSize[0]*ku/(1.1*maxNumberOfNodes)),int(screenSize[1]*0.8)))


stepsFactor = 1
nodesList = []     #list of all nodes (including the ones that are not yet on the main list)
nodesOnMainList = []

workersDict = dict()

insertersWaitingList = []
deletersWaitingList = []
searchersWaitingList = []

#---------------------------------------command processing function-----------------------------------------
#command={'action','thr_id','class','args'*}
# the argument 'args' is optional, when it's required, it can be accessed using the argument name itself, ex: command['index']
#  Actions table

#  class |  actions     |  args
#-------------------------------
#   all  |  leave       |   -
#   all  |  isWaiting   |   -
#    s   |  next        |   -
#    d   |  moveTowards | index
#    i   |  move Towrds |   -    

def processCommand(command):
    if command['action'] == 'leave':
        workersDict[command['thr_id']].goAway()

    elif command['action'] == 'isWaiting':
        if command['class'] == 'i':
            workersDict[command['thr_id']] = Inserter()
        elif command['class'] == 's':
            workersDict[command['thr_id']] = Searcher()
        elif command['class'] == 'd':            
            workersDict[command['thr_id']] = Deleter()

        workersDict[command['thr_id']].thr_id = command['thr_id']
        workersDict[command['thr_id']].wantsToWork()

    elif command['action'] == 'next':
        if command['class'] != 's':
            print "Error: Only searchers have the 'next' command"
        workersDict[command['thr_id']].goToNextNode()
        
    elif command['action'] == 'moveTowards':
        if command['class'] == 'i':
            workersDict[command['thr_id']].insertNodeOnList()
        else:
            workersDict[command['thr_id']].deleteNodeFromList(command['index'])
        
    

#---------------------------------------------helper functions-----------------------------------------------

#loads an image
def load_image(name, colorkey=None):
    fullname = os.path.join('../images/', name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error, message:
        print 'Cannot load image:', fullname
        raise SystemExit, message
    image = image.convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
        image.set_colorkey(colorkey, RLEACCEL)
    return image, image.get_rect()
    
#returns the list of all points the object should go
def calculatePath(startingPoint,endingPoint):    
    pathVertex = list()
    pathVertex.append((startingPoint[0],startingPoint[1]))
    pathVertex.append((startingPoint[0],startingPoint[1]+ (endingPoint[1] - startingPoint[1])*0.1))
    pathVertex.append((endingPoint[0],  startingPoint[1]+ (endingPoint[1] - startingPoint[1])*0.1))
    pathVertex.append((endingPoint[0],endingPoint[1]))
    
    pathSteps = list() 
    
    for i in range(3):
        stepsNumber = max( abs(pathVertex[i+1][0]-pathVertex[i][0]), abs(pathVertex[i+1][1]-pathVertex[i][1]))*stepsFactor
        stepx,stepy = 0,0
        if(stepsNumber !=0):
            stepx = float(pathVertex[i+1][0]-pathVertex[i][0])/stepsNumber
            stepy = float(pathVertex[i+1][1]-pathVertex[i][1])/stepsNumber
        
        for j in range(int(stepsNumber)+1):
            pathSteps.append((int(pathVertex[i][0] + stepx*j), int(pathVertex[i][1] + stepy*j)))
    pathSteps.pop(0) 
    return pathSteps
    
#----------------------------------------------inserters-----------------------------------------------------------
class Inserter(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self) #call Sprite initializer
        self.image, self.rect = load_image('circleYellow.png', -1)
        self.thr_id = -1;
        self.rect.midtop = (-50,-50)
        self.nextPosition = [self.rect.midtop]
        self.stepCounter = 0
        self.finishedAnimatingWaiting = 0
        self.finishedAnimatingInserting = 0
        self.startedAnimatingWaiting = 0
        self.startedAnimatingInserting = 0
        self.startedAnimationLeaving = 0
        self.node = None
        allSprites.add(self)



    def update(self):        
        if(self.rect.midtop[0] > screenSize[0]*1.05 and self.startedAnimationLeaving == 1):
            del workersDict[self.thr_id]
            allSprites.remove(self)
            
        
        if(self.finishedAnimatingInserting):
            
            self.finishedAnimatingInserting = 0
            nodesOnMainList.append(self.node)
            self.node.following = None
            
            
        
        if((self.stepCounter<len(self.nextPosition))and ((self.nextPosition[self.stepCounter] != self.rect.midtop) or self.stepCounter!=len(self.nextPosition))):
            self.rect.midtop = self.nextPosition[self.stepCounter]
            self.stepCounter = self.stepCounter + 1
        elif self.startedAnimatingWaiting:
            
            self.startedAnimatingWaiting = 0
            self.finishedAnimatingWaiting = 1
            sharedVariables.animation_locks[self.thr_id].signal()
        elif self.startedAnimatingInserting:
            sharedVariables.animation_locks[self.thr_id].signal()
            
            self.startedAnimatingInserting = 0
            self.finishedAnimatingInserting = 1
                 
    def initializeThreadID(self,thr_id):
        self.thr_id = thr_id

    def wantsToWork(self):
        
        self.startedAnimatingWaiting = 1
        insertersWaitingList.append(self)
        waitingIndex = insertersWaitingList.index(self)
        myNode = Node()
        myNode.following = self
        self.node = myNode
        self.moveToPosition(insertersWaitingPositons[waitingIndex][0],insertersWaitingPositons[waitingIndex][1])
        
    def moveToPosition(self,newX,newY):
        newPosition = (newX,newY)
        newPath = calculatePath(self.rect.midtop,newPosition)
        self.stepCounter = 0
        self.nextPosition = newPath

    def insertNodeOnList(self):
        myIndex = insertersWaitingList.index(self)
        insertersWaitingList.remove(self)
        for ins in insertersWaitingList:
            insIndex = insertersWaitingList.index(ins)
            if insIndex >= myIndex:
                ins.moveToPosition(insertersWaitingPositons[insIndex][0],ins.rect.midtop[1])

        self.startedAnimatingInserting = 1
        insertIndex = len(nodesOnMainList)
        self.moveToPosition(nodesPositions[insertIndex][0],nodesPositions[insertIndex][1])

    def goAway(self):
        self.startedAnimationLeaving = 1
        self.moveToPosition(inserterEscapingPosition[0],inserterEscapingPosition[1])


#----------------------------------------------deleters------------------------------------------------------------       

class Deleter(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self) #call Sprite initializer
        self.image, self.rect = load_image('circleRed.png', -1)
        self.thr_id = -1;
        self.rect.midtop = (screenSize[0]/10,screenSize[1]*1.1)
        self.nextPosition = [self.rect.midtop]
        self.stepCounter = 0
        self.finishedAnimatingWaiting = 0
        self.finishedAnimatingDeleting = 0
        self.startedAnimatingWaiting = 0
        self.startedAnimatingDeleting = 0
        self.node = None
        self.nodeIndex = -1
        self.startedAnimationLeaving = 0
        allSprites.add(self)

    def update(self):
        if(self.rect.midtop[0] > screenSize[0]*1.05 and self.startedAnimationLeaving == 1):
            del workersDict[self.thr_id]
            allSprites.remove(self)
        
        if(self.finishedAnimatingDeleting):
            self.finishedAnimatingDeleting = 0
            self.node = nodesOnMainList[self.nodeIndex]
            nodesOnMainList.remove(self.node)
            self.node.following = self
            
        
        if((self.stepCounter<len(self.nextPosition))and ((self.nextPosition[self.stepCounter] != self.rect.midtop) or self.stepCounter!=len(self.nextPosition))):
            self.rect.midtop = self.nextPosition[self.stepCounter]
            self.stepCounter = self.stepCounter + 1
        elif self.startedAnimatingWaiting:
            sharedVariables.animation_locks[self.thr_id].signal()

            self.startedAnimatingWaiting = 0
            self.finishedAnimatingWaiting = 1
        elif self.startedAnimatingDeleting:
            self.startedAnimatingDeleting = 0
            self.finishedAnimatingDeleting = 1
            sharedVariables.animation_locks[self.thr_id].signal()
                 
    def initializeThreadID(self,thr_id):
        self.thr_id = thr_id

    def wantsToWork(self):
        self.startedAnimatingWaiting = 1
        deletersWaitingList.append(self)
        waitingIndex = deletersWaitingList.index(self)
        self.moveToPosition(deletersWaitingPositions[waitingIndex][0],deletersWaitingPositions[waitingIndex][1])
        
    def moveToPosition(self,newX,newY):
        newPosition = (newX,newY)
        newPath = calculatePath(self.rect.midtop,newPosition)
        self.stepCounter = 0
        self.nextPosition = newPath

    def deleteNodeFromList(self,nodeIndex):
        myIndex = deletersWaitingList.index(self)
        deletersWaitingList.remove(self)
        for dele in deletersWaitingList:
            deleIndex = deletersWaitingList.index(dele)
            if deleIndex >= myIndex:
                dele.moveToPosition(deletersWaitingPositions[deleIndex][0],dele.rect.midtop[1])

        self.nodeIndex = nodeIndex
        self.startedAnimatingDeleting = 1
        self.moveToPosition(nodesPositions[nodeIndex][0],nodesPositions[nodeIndex][1])

    def goAway(self):
        self.startedAnimationLeaving = 1
        self.moveToPosition(deleterEscapingPosition[0],deleterEscapingPosition[1])
        for node in nodesOnMainList:
            myIndex = nodesOnMainList.index(node)
            if myIndex >= self.nodeIndex:
                node.moveToPosition(nodesPositions[myIndex][0],node.rect.midtop[1])
        
        


#----------------------------------------------searchers-----------------------------------------------------------



class Searcher(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self) #call Sprite initializer
        self.image, self.rect = load_image('circleGreen.png', -1)
        self.thr_id = -1;
        self.rect.midtop = (-50,(random.randrange(1,6)*(screenSize[1]*0.01))+screenSize[1]*0.51)
        self.nextPosition = [self.rect.midtop]
        self.stepCounter = 0
        self.startedAnimatingWaiting = 0
        self.finishedAnimatingWaiting = 0
        self.startedAnimatingGoingToFirstNode = 0
        self.finishedAnimatingGoingToFirstNode = 0
        self.startedAnimatingGointToNextNode = 0
        self.finishedAnimatingGointToNextNode = 0
        self.startedAnimationLeaving = 0
        self.nodeIndex = -1
        allSprites.add(self)
        
    def update(self):
        
        if(self.rect.midtop[0] > screenSize[0]*1.05 and self.startedAnimationLeaving == 1):
            del workersDict[self.thr_id]
            allSprites.remove(self)
            
           
        
        if((self.stepCounter<len(self.nextPosition))and ((self.nextPosition[self.stepCounter] != self.rect.midtop) or self.stepCounter!=len(self.nextPosition))):
            self.rect.midtop = self.nextPosition[self.stepCounter]
            self.stepCounter = self.stepCounter + 1
        elif self.startedAnimatingWaiting:
            self.startedAnimatingWaiting = 0
            sharedVariables.animation_locks[self.thr_id].signal()
        elif self.startedAnimatingGoingToFirstNode:
            self.startedAnimatingGoingToFirstNode  = 0
            sharedVariables.animation_locks[self.thr_id].signal()
        elif self.startedAnimatingGointToNextNode:
            self.startedAnimatingGointToNextNode = 0
            sharedVariables.animation_locks[self.thr_id].signal()
                 
    def initializeThreadID(self,thr_id):
        self.thr_id = thr_id
    def wantsToWork(self):
        self.startedAnimatingWaiting = 1
        searchersWaitingList.append(self)
        self.moveToPosition(screenSize[0]*0.1,(random.randrange(1,6)*(screenSize[1]*0.01))+screenSize[1]*0.51)
        
    def moveToPosition(self,newX,newY):
        newPosition = (newX,newY)
        newPath = calculatePath(self.rect.midtop,newPosition)
        self.stepCounter = 0
        self.nextPosition = newPath
    
    def goToFirstNode(self):
        self.startedAnimatingGoingToFirstNode = 1
        self.nodeIndex = 0
        self.moveToPosition(nodesPositions[self.nodeIndex][0],self.rect.midtop[1])

    def goToNextNode(self):
        if self.nodeIndex == -1:
            self.goToFirstNode()
        else:
            self.startedAnimatingGointToNextNode = 1
            self.nodeIndex = self.nodeIndex + 1
            self.moveToPosition(nodesPositions[self.nodeIndex][0],self.rect.midtop[1])

    def goAway(self):
        self.startedAnimationLeaving = 1
        self.moveToPosition(searcherEscapingPosition[0],searcherEscapingPosition[1])





#------------------------------------------------Nodes-------------------------------------------------------------
class Node(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self) #call Sprite initializer
        self.image, self.rect = load_image('node.png', -1)
        self.following = None
        self.rect.midtop = (-50,-50)
        self.nextPosition = [self.rect.midtop]
        self.stepCounter = 0
        nodesList.append(self)
        allSprites.add(self)
    def update(self):
        
        if((self.stepCounter<len(self.nextPosition))and ((self.nextPosition[self.stepCounter] != self.rect.midtop) or self.stepCounter!=len(self.nextPosition))):
            self.rect.midtop = self.nextPosition[self.stepCounter]
            self.stepCounter = self.stepCounter + 1
        
        if(self.following):
            self.rect.midtop = (self.following.rect.midtop[0],self.following.rect.midtop[1]+self.following.rect.height)
                 
    def moveToPosition(self,newX,newY):
        newPosition = (newX,newY)
        newPath = calculatePath(self.rect.midtop,newPosition)
        self.stepCounter = 0
        self.nextPosition = newPath
    




def runTheGame():
    #Initialize Everything
    pygame.init()
    screen = pygame.display.set_mode(screenSize)
    pygame.display.set_caption('Search-interst-delete')

    #Create The Backgound
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((0, 0, 0))
    
    #create the legend
    legend = pygame.image.load('../images/legendaSO.png')



    #Display The Background
    screen.blit(background, (0, 0))
    
    pygame.display.flip()


    #main loop
    while 1:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: 
                os._exit(0)

        #Update Objects
        
        allSprites.update()
        #Draw Everything
        screen.blit(background, (0, 0))
        allSprites.draw(screen)
        screen.blit(legend,(screenSize[0]-293,screenSize[1]-127))
        pygame.display.flip()

        while(1):
            item = sharedVariables.getNextCommand()
            if item == None:
                break;
            processCommand(item)




