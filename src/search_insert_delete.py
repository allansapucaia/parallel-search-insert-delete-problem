#!/usr/bin/python

#This file contains the main logic used to solve the "Searchers-Inserters-Deleters Problem", it uses animation locks to syncronized with a external interface in a "Multiple-Producerts Single-Consumer" like relashion.

import time, random, sys,os
import sharedVariables
import animation

from threading_cleanup import *
from lightswitch import *

#global variables
all_mutex = Semaphore(1) #main lock, prevent any worker to enter
empty_mutex = Semaphore(1) #lock associated with the lighswitch, locks while there is some one working
insert_mutex = Semaphore(1) #lock when an inserter is working

empty_switch = Lightswitch() #signal when the critical zone is free, locking otherwise


#shared list
share_data = []; #main list

#state
working = [] #list of working threads (class,id)
waiting = [] #list of waiting threads (class,id)

#counters
counter_mutex = Semaphore(1)
inserters_count = 0
deleters_count = 0
searchers_count = 0

state_mutex = Semaphore(1) #lock the state printing procedure

#atomic add a value to a variable
def atomicAdd(var,val):
    counter_mutex.wait()
    var += val
    counter_mutex.signal()
    return var

#prints current state
def printState():
    state_mutex.wait()
    print "Waiting:"
    print waiting
    print "Working:"
    print working
    print "List:"
    print share_data
    state_mutex.signal()

# Threads
#inserter thread
def inserter(i):
    global inserters_count
    inserters_count = atomicAdd(inserters_count,1)
    waiting.append(('i',i))

    # add commands and locks animation    
    lock_animation = Semaphore(0)
    sharedVariables.animation_locks[i] = lock_animation

    sharedVariables.insertCommand({'action':'isWaiting', 'class':'i', 'thr_id':i})
    print "(%d - i) esta esperando a animacao de entrada"%(i)
    lock_animation.wait()

    print "(%d - i) esta esperando o lock geral"%(i)
    #the inserter lock the semaphore, updates the lighswitch and unlock the semaphore(guarantees mutual-exclusion between inserters and deleters) 
    all_mutex.wait()
    empty_switch.lock(empty_mutex)
    all_mutex.signal()

    #guarantees mutual-exclusion between inserters
    print "(%d - i) esta esperando que outro inserter saia"%(i)
    insert_mutex.wait()
    working.append(('i',i))
    rand_element = random.randint(0, 20)
    # change status and locks animation
    sharedVariables.insertCommand({'action':'moveTowards', 'class':'i', 'thr_id':i})
    print "(%d - i) esta o lock da animacao de entrada"%(i)
    lock_animation.wait()
    print "(%d - i) comecou a inserir"%(i)
    #actual list's update
    share_data.append(rand_element);

    time.sleep(1)
    print '(%d - i) terminou de inserir'%(i)

    working.remove(('i',i))
    
    # change status and locks animation
    sharedVariables.insertCommand({'action':'leave', 'class':'i', 'thr_id':i})
    print "(%d - i) esta o lock da animacao de saida"%(i)

    inserters_count = atomicAdd(inserters_count,-1)
    #lock_animation.wait()

    #unlock the inserter mutex and update the lighswitch 
    insert_mutex.signal()
    empty_switch.unlock(empty_mutex)
    del sharedVariables.animation_locks[i]
    print "(%d - i) saiu"%(i)

#searcher thread
def searcher(i):
    waiting.append(('s',i))
    global searchers_count
    atomicAdd(searchers_count,1)

    # add commands ans locks animation
    lock_animation = Semaphore(0)
    sharedVariables.animation_locks[i] = lock_animation

    sharedVariables.insertCommand({'action':'isWaiting', 'class':'s', 'thr_id':i})
    print "(%d - s) esta esperando a animacao de entrada"%(i)
    lock_animation.wait()

    #the inserter lock the semaphore, updates the lighswitch and unlock the semaphore(guarantees mutual-exclusion between inserters and deleters) 
    print "(%d - s) esta esperando o lock geral"%(i)
    all_mutex.wait()
    empty_switch.lock(empty_mutex)
    all_mutex.signal()
    working.append(('s',i))
    rand_element = random.randint(0, 20)
    curPos = 0
    found = False
    
    print "(%d - s) comecou a buscar"%(i)
    while curPos < len(share_data) and (not found):
        # change status ans locks animation
        sharedVariables.insertCommand({'action':'next', 'class':'s', 'thr_id':i})
        print "(%d - s) esta esperando o lock de animacao"%(i)
        lock_animation.wait()
        print "(%d - s) esta procurando no elemento de indice %d"%(i,curPos)
        time.sleep(1)
        if share_data[curPos] == rand_element:
            print "(%d - s) nao encontrou o elemento"%(i)
            found = True
        else:
             print "(%d - s) encontrou o elemento"%(i)
        curPos+=1
    working.remove(('s',i))
    print "(%d - s) terminou de buscar"%(i)
    # change status ans locks animation
    sharedVariables.insertCommand({'action':'leave', 'class':'s', 'thr_id':i})
    print "(%d - s) esta o lock da animacao de saida"%(i)
    atomicAdd(searchers_count,-1)
    searchers_count -= 1
    #lock_animation.wait()

    #updates the lighswitch
    empty_switch.unlock(empty_mutex)
    del sharedVariables.animation_locks[i]
    print "(%d - s) saiu"%(i)

#deleter threads
def deleter(i):
    waiting.append(('d',i))
    global deleters_count
    deleters_count = atomicAdd(deleters_count,1)
    # add commands and locks animation
    lock_animation = Semaphore(0)
    sharedVariables.animation_locks[i] = lock_animation

    sharedVariables.insertCommand({'action':'isWaiting', 'class':'d', 'thr_id':i})
    print "(%d - d) esta esperando a animacao de entrada"%(i)
    lock_animation.wait()

    #lock the main semaphore, so when the deleter's waiting starts, new elements are locked, this guarantees that no deleter starves
    print "(%d - d) esta esperando o lock geral"%(i)
    all_mutex.wait()
    #waits until all the workers leave
    print "(%d - d) esta esperando os outros sairem"%(i)
    empty_mutex.wait()
    working.append(('d',i))
    if len(share_data) > 0:
        rand_index = random.randint(0, len(share_data)-1)
        # change status and locks animation
        sharedVariables.insertCommand({'action':'moveTowards', 'class':'d', 'thr_id':i,'index':rand_index})
        lock_animation.wait()

        print '(%d - d) comecou a deletar o elemento na posicao %d' %(i, rand_index)
        time.sleep(1)
        del share_data[rand_index]
    
        print '(%d - d) terminou de deletar o elemento na posicao %d' %(i, rand_index)
        
    working.remove(('d',i))

    # change status and locks animation
    sharedVariables.insertCommand({'action':'leave', 'class':'d', 'thr_id':i})
    print "(%d - d) esta o lock da animacao de saida"%(i)
    deleters_count = atomicAdd(deleters_count,-1)
    
    #unlock both semaphores, allowing new workers to enter
    del sharedVariables.animation_locks[i]
    empty_mutex.signal()
    all_mutex.signal()
    print '(%d - d) saiu' %(i)
# main
def main():
    #random test
    global inserters_count
    global deleters_count
    global searchers_count

    
    if len(sys.argv) < 5:
        print "This software requires the following parameters( in the given order):"
        print "\n\tnumWorkers pInserters pDeleter maxSizeList\n"
        print "\t0 < numWorkers: Number of workers"
        print "\t0 <= pInserters+pDeleter < 100: percentage distribuition"
        print "\t0 < maxSizeList <= 5: list's maximum size"
        os._exit(0)

    numWorkers = int(sys.argv[1]) 
    pInserter = int(sys.argv[2])
    pDeleter = int(int(sys.argv[3]))
    maxSizeList = int(int(sys.argv[4]))

    if numWorkers < 0:
        print "The number of workers is invalid, see instructions"
        os._exit(1)
    if pInserter < 0 or pDeleter < 0 or pInserter+pDeleter > 100:
        print "The probability distribution is invalid, see instructions"
        os._exit(1)
    if maxSizeList > 5:
        print "List's maximum size is invalid, see instructions"
        os._exit(1)

    for i in range(numWorkers):
        time.sleep(1+random.random())
        p = random.randint(0,100)
        counter_mutex.wait()
        if(p <  pInserter and inserters_count + len(share_data) < maxSizeList):
            Thread(inserter, i)
        elif(p < pInserter+pDeleter and  deleters_count < len(share_data) and deleters_count < maxSizeList):
            Thread(deleter, i)
        else: 
            Thread(searcher, i) 
        counter_mutex.signal()
    sharedVariables.done = 1
    return 0

if __name__ == '__main__':
    Thread(main)
    animation.runTheGame()
    
