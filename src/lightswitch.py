#!/usr/bin/python

from threading_cleanup import *

class Lightswitch:
    """ the first person into a room turns on the light (locks the mutex)
    and the last one out turns it off (unlocks the mutex) """

    def __init__(self):
        self.counter = 0
        self.mutex = Semaphore(1)

    def lock(self, semaphore):
        self.mutex.wait()
        self.counter += 1
        if self.counter == 1:
            semaphore.wait()
        self.mutex.signal()

    def unlock(self, semaphore):
        self.mutex.wait()
        self.counter -= 1
        if self.counter == 0:
            semaphore.signal()
        self.mutex.signal()
