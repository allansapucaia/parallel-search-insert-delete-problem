#GRUPO:	
	Nome                       RA
	Allan Sapucaia Barboza     134796
	Diego Luis de Souza        135491
	Luis Fernando Antonioli	   136710
	 


##Descrição do problema
*****************************
Três tipos de threads compartilham acesso a uma lista ligada: Searchers, inserters e deleters. Searchers podem acessar a lista concorrentemente com outros searchers. As inserções sempre são feitas no fim da lista, e devem ser mutuamente exclusivas para evitar que dois inserters adicionem um elemento ao mesmo tempo. Entretanto, um inserter pode trabalhar em paralelo com qualquer número de searchers. Os deleters podem remover um item de qualquer lugar da lista. No máximo um deleter acessa a lista ao mesmo tempo, e cada remoção deve ser mutuamente exclusiva com inserções e pesquisas.
O problema foi retirado do livro [The Little Book of Semaphores](http://greenteapress.com/semaphores/), pag. 165.


##Solução
**********************
Nossa solução diverge da proposta no [livro](http://greenteapress.com/semaphores/) por escolher evitar starvation em troca de reduzir o paralelismo. Em muitos problemas com diversas threads e, principalmente, classes é preciso escolher qual dos dois aspéctos priorizar. Na solução apresentada no livro, um deleter pode esperar indefinidamente enquanto chegam novos inserters e searchers. Já nossa solução consiste em deixar todos os trabalhadores entrarem na região crítica a medida que chegam. Os inserters são limitados a trabalhar apenas um por vez e os deleters impedem a entrada de outros trabalhores enquando estiverem na região crítica e só começam a trabalhar após todos os outros saírem. A corretude do algoritmo é indicada em pontos chaves no código-fonte.

Além disso, escolhemos tornar a solução independente da parte gráfica. A comunicação entre elas é feita através de uma fila de comandos que são inseridos pelas threads, quando querem executar alguma animação, e consumidos pelo módulo gráfico, de maneira similar ao problema de múltiplos produtores e consumidor único. Para garantir o sincronismo entre os dois módulos, cada thread possui um semáforo de animação e após inserir um comando na fila, espera que o módulo gráfico de signal no mesmo.

##Parâmetros e implementação
**********************
A implementação foi dividida em três arquivos:

* [search_insert_delete.py](https://bitbucket.org/allansapucaia/search-insert-delete-problem-animation/src/765ab54777f21b9951ce40c31db4b3f85e31dd75/src/search_insert_delete.py): possui as threads e resolve efetivamente o problema.

* [animation.py](https://bitbucket.org/allansapucaia/search-insert-delete-problem-animation/src/765ab54777f21b9951ce40c31db4b3f85e31dd75/src/animation.py): responsável pela animação.

* [shareVariables.py](https://bitbucket.org/allansapucaia/search-insert-delete-problem-animation/src/765ab54777f21b9951ce40c31db4b3f85e31dd75/src/sharedVariables.py): contem uma fila de instruções e um semáforo para cada thread. Esse arquivo é compartilhado entre search_insert_delete.py e animation.py, permitindo a comunicação entre as threads e o código da animação.

* [threading_cleanup.py](http://greenteapress.com/semaphores/threading_cleanup.py): possui classes para manipulação de threads e semáforos em python. A implementação foi obtida a partir de uma referência do [livro](http://greenteapress.com/semaphores/) que contem a descrição do problema.

* [lightswitch.py](https://bitbucket.org/allansapucaia/search-insert-delete-problem-animation/src/765ab54777f21b9951ce40c31db4b3f85e31dd75/src/lightswitch.py): Implementa a ideia do primeiro que entra na sala acende a luz (bloqueia o mutex) e o último que sai desliga (libera o mutex). O [livro](http://greenteapress.com/semaphores/) mostra uma implementação na pag. 76. 


Para simulação do problema digite em um terminal:

	python search_insert_delete.py [p1] [p2] [p3] [p4]

* [p1]: Número de workers.
* [p2]: Porcentagem aproximada de inserters.
* [p3]: Porcentagem aproximada de deleters.
* [p4]: Tamanho máximo da lista.

